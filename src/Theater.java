import java.util.ArrayList;
import java.util.LinkedList;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Theater
{
	public ArrayList<BoxOffice> offices;
	public static int TotalSeats=28*26+23;
	public static int seatCount = TotalSeats;
	public static int numPeople = 2000;
	public LinkedList<Seat> seatQueue;
	private Lock seatFindLock = new ReentrantLock();
	private Condition sufficientSeatsCondition = seatFindLock.newCondition();

		/**generateSeats generates the seatqueue by priority
		 * seats are going to be generated in rows and columns
		 * rows are 28 columns wide, 
		 * totalRows= (numSeats-6)/28
		 * first row is only 22 seats
		 * columns are 101-128 
		 * home left is 101-107
		 * middle is 108-121
		 * home right is 122-128
		 * seats are assigned priority for being in the first 10 rows
		 * and in middle, then sides, then back middle, then back sides
		 * 
		 * @param numSeats the total number of seats
		 */
	public Theater(){
		seatQueue = generateSeats();
	//	System.out.println("Created new theatre");
	}
	
	/**bestAvailableSeat finds the best available seat
	 * 
	 * @return Seat that is the bestAvailableSeat
	 */
	public Seat bestAvailableSeat()
	{
		Seat s= seatQueue.get(0);
		int x=1;
		while(s.isReserved()&& x<seatQueue.size())
		{
			s=seatQueue.get(x++);
		}
		if(x>=seatQueue.size())
		{
		return null;
		}
		//System.out.println(s);
	
		return s;
	}
	
	/**markAvailableSeat marks the best available seat
	 * 
	 *
	 */
	public void markAvailableSeat(Seat a) throws InterruptedException
	{	
		if(a == null)
		{
			return;
		}
		seatFindLock.lock();
		
		try{
//		while(a == null) sufficientSeatsCondition.await();
			if(!a.isReserved()){
		a.reserve();//reserves the seat
		seatCount--;
			}
			else
			{
				System.out.println("Failed to reserve " + a.printSeatTicket());
				throw new InterruptedException();
				
			}
		//System.out.print("...");
		}
		
		finally
		{
			seatFindLock.unlock();
		}
	}
	
	/**printAvailableSeat prints the ticket for the best available seat
	 * 
	 * 
	 */
	
	public void printTicketSeat(String name, Seat a)
	{
		if(a == null)
		{
			return;
		}
		else
		{
		
			System.out.println(name+": " + a.printSeatTicket()); //prints the ticket for the seat.	
		}
		
		//	Input: seat is the location of an available seat in the theater.
		//	output: A ticket for that seat is printed to the screen � leave it on the screen long enough to be read easily by the client. The output format is up to you, but should contain the essential information found on a theater ticket.

	}
	
	public LinkedList<Seat> generateSeats()//int totalSeats)
	{
		LinkedList<Seat> s=new LinkedList<Seat>();
		int numSeats= 99999;//26*28+22;
		
		//populate first row
		for(int x=108; x<122&&numSeats>0; x++)
		{
			s.add(new Seat("A",x,"M"));
			numSeats--;
		}
		for(int x=104; x<108&&numSeats>0; x++)
		{
			s.add(new Seat("A",x,"HR"));
			numSeats--;
		}
		for(int x=122; x<126&&numSeats>0; x++)
		{
			s.add(new Seat("A",x,"HL"));
			numSeats--;
		}
		
		//populate next rows, B-K middle
		for(int y=1; y<10; y++)//second priority
		{
			for(int x=108; x<122&&numSeats>0; x++)
			{
				s.add(new Seat(((char)(y+'A'))+"",x,"M"));
				numSeats--;
			}
		}
		
		for(int y=1; y<10; y++)//third priority, cols B-K home left right
		{
			for(int x=101; x<108&&numSeats>0; x++)
			{
				s.add(new Seat(((char)(y+'A'))+"",x,"HR"));
				numSeats--;
			}
			for(int x=122; x<129&&numSeats>0; x++)
			{
				s.add(new Seat(((char)(y+'A'))+"",x,"HL"));
				numSeats--;
			}
		}
		
		//now rows L-AA
		for(int y=10; y<27; y++)//fourth priority
		{
			for(int x=108; x<122&&numSeats>0; x++)
			{
				if(y<26)
				s.add(new Seat(((char)(y+'A'))+"",x,"M"));
				else
				s.add(new Seat("AA",x,"M"));
				
				numSeats--;
			}
		}
		
		for(int y=10; y<27; y++)//fifth priority
		{
			for(int x=101; x<108&&numSeats>0; x++)
			{
				if(y<26)
				s.add(new Seat(((char)(y+'A'))+"",x,"HR"));
				else
				s.add(new Seat("AA",x,"HR"));
				
				numSeats--;
			}
			for(int x=122; x<129&&numSeats>0; x++)
			{
				if(y<26)
				s.add(new Seat(((char)(y+'A'))+"",x,"HL"));
				else
				s.add(new Seat("AA",x,"HL"));
				
				numSeats--;
			}
		}
		return s;
	}




}