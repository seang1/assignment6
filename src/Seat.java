//import java.util.concurrent.locks.Condition;
//import java.util.concurrent.locks.Lock;
import java.util.*;

public class Seat {
	private int col;
	private boolean reserved;
	private String loc;
	private String row;
	// add lock and condition stuff here
	
public Seat()
{
	
}

public Seat(String r, int c, String l)
{
	row=r;
	col=c;
	loc=l;
	reserved=false;
	
}

public void reserve()// multi thread this thingy??
{
	reserved=true;
}

public String getRow()
{
	return row;
}
public int getCol()
{
	return col;
}
public String getLocation()
{
	return loc;
}

public void getInfo()
{
	System.out.println("row: " + this.getRow() + " - col: " + this.getCol() + " - loc: " + this.getLocation());
}
/** 
 * 
 * @return true if reserved, false otherwise
 */
public boolean isReserved() // returns true if NOT taken
{
	return reserved;
}
public String printSeatTicket()
{
	StringBuffer s=new StringBuffer();
	s.append(loc+", ");
	s.append(col);
	s.append(row);
	return s.toString();
}
}
