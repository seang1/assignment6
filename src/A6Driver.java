/**A6Driver
 * Sean Gajjar and Matthew Speller
 * sg34324, mds2968
 * Monday 4:30-6
 * 
 * Driver for assignment 6
 */

import java.util.*;

public class A6Driver
{


public static void main(String [] cheese) throws InterruptedException
{
//	A6Driver A=new A6Driver();
	
	Theater a = new Theater();
	Runnable ab = new BoxOffice(a, "Box Office 1: ");

	
	Runnable ac = new BoxOffice(a, "Box Office 2: ");

	Thread t1 = new Thread(ab);
	Thread t2 = new Thread(ac);
	
	t1.start();
	t2.start();


}


}